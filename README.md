# firebirdsPractice

Chapter 1{ 

It was a complicated mission that they were about to depart on. For the great county of Latvia, they were to infiltrate a high-security base run by the government of the United States of America. All the two agents were given were coordinates and a tracking fob. The objective was to return with one of the most valuable relics of the history of the modern world: the frozen body of United States President William Howard Taft. The two agents being sent were the highest in skill known to man. Remy, quick and nimble, skilled in mind control. The other, Mulan, the woman (or man?) with a thousand identities. This was set up to be an almost impossible task to fulfill, yet they were  up to the challenge.

The two arrived via 2004 Ford Fiesta, and when the two were finally able to find the location of the coordinates, they were astonished. A Taco Bell in the middle of Nashville, Tennessee. Even more intimidating than they expected. However, they had to follow through with their mission for the great nation of Latvia. Just how would they accomplish this? The answer was right in front of them: they would go undercover and become employed as Taco Bell staff. And when the workers least expected it, they would break into the freezer and bail out the greatest weapon of the modern era: President Taft.

}

Chapter 2{

Remy and Mulan mixed the steaming pot as they threw the ingredients in one by one. The war raged on behind them. The French and Chinese were going at it. "Where's the paprika?" Mulan asked. "It should be in the tent" Remy replied. With a deep sigh, Mulan ran over to the tent and snatched the paprika. As she turned around a bullet flew right by her. A French soldier ran towards her. She had been training for this very moment. Mulan threw the paprika up in the air, drop kicked the guy kicked the spice thirty yards back toward remy as it came back down. Remy secured the paprika and added just a pinch into the pot. "Perfect!" he exclaimed. "The dish is complete!" Mulan ran back towards the pot, dodging bullets and right hooking anyone that got in her way. Suddenly, a piercing pain shot through her arm. "Mulan!" Remy called. "Go on! Go on without me!" she called to him. Remy had no choice. He filled a bowl with the beautiful stew they prepared and yelled out victoriously "ANYONE CAN COOK!" The French and the Chinese stopped fighting and grew silent. Mulan trudged back towards the pot and served a bowls to the soldiers. "This... this is the best stew I've ever tasted..." A Chinese soldier said. "I... I agree..." A French soldier said. They looked each other deep in the eyes. "Maybe... we're not so different after all..." The Chinese soldier said. "Oui..." The French replied. The hugged each other and everyone was served some stew. And thus world peace reigned until they all died from the irreversible effects of global warming.


}


Chapter 3{
    After the monstrosity that they have created, they looked at each other with excitement in their eyes. Then there was an explosion in the distance, a huge cloud of smoke. Mulan was startled as the shockwave blew her back. Colors separated from themselves in her eyes. The ringing in her ear was deafening. She called out to Remi, but she couldn’t hear her own voice. Slowly she got up, wincing in pain. The multiple images she saw became one. She looked around.

Remi was gone.

Mulan was horrified. She cried out to Remi, begging God to let her see him again. She would do anything to be able to see him again.

“Your wish is my command.”

Suddenly, she felt a sharp pain in her spine. She collapsed to the floor. She tried to run, but she couldn’t feel her legs. They were dead and numb. All she could do was slowly crawl away.She turned around.

It was Remi, standing over her. In his hands was a bloody baguette. And as Mulan started to slip into unconsciousness, she heard Remi whisper to her one final phrase.

“Who wants Ratatouille?”

To be continued...
}
